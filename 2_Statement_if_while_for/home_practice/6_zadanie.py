# (цикл for)
import simple_draw as sd
sd.resolution = (600, 600)
# Нарисовать стену из кирпичей. Размер кирпича - 100х50
# Использовать вложенные циклы for
x1, y1, x2, y2 = 0, 0, 100, 50
point1 = sd.get_point(x1, y1)
point2 = sd.get_point(x2, y2)

for i in range(1, 13):
    if i % 2 != 0:
        x1 -= 50
        x2 -= 50
        point1 = sd.get_point(x1, y1)
        point2 = sd.get_point(x2, y2)
        for j in range(7):
            sd.rectangle(left_bottom=point1, right_top=point2, color=sd.COLOR_YELLOW, width=1)
            x1 += 100
            point1 = sd.get_point(x1, y1)
            x2 += 100
            point2 = sd.get_point(x2, y2)
    else:
        for j in range(6):
            sd.rectangle(left_bottom=point1, right_top=point2, color=sd.COLOR_YELLOW, width=1)
            x1 += 100
            point1 = sd.get_point(x1, y1)
            x2 += 100
            point2 = sd.get_point(x2, y2)
    x1 = 0
    y1 = y1 + 50
    x2 = 100
    y2 = y2 + 50

# Подсказки:
#  Для отрисовки кирпича использовать функцию rectangle
#  Алгоритм должен получиться приблизительно такой:
#
#   цикл по координате Y
#       вычисляем сдвиг ряда кирпичей
#       цикл координате X
#           вычисляем правый нижний и левый верхний углы кирпича
#           рисуем кирпич


sd.pause()
