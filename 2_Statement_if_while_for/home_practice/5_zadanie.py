import simple_draw as sd
from simple_draw import Point

sd.resolution = (1200, 700)

rainbow_colors = (sd.COLOR_RED, sd.COLOR_ORANGE, sd.COLOR_YELLOW, sd.COLOR_GREEN,
                  sd.COLOR_CYAN, sd.COLOR_BLUE, sd.COLOR_PURPLE)

#def rainbow(x_point, y_point, color):
#    sd.line(start_point=x_point, end_point=y_point, color=color, width=25)

#x = 50
#y = 350
#x_point = sd.get_point(x, 50)
#y_point = sd.get_point(y, 450)


# Нарисовать радугу: 7 линий разного цвета толщиной 4 с шагом 5 из точки (50, 50) в точку (350, 450)
#for i in rainbow_colors:
#    rainbow(x_point, y_point, i)
#    x += 25
#    y += 25
#    x_point = sd.get_point(x, 50)
#    y_point = sd.get_point(y, 450)
# Подсказка: цикл нужно делать сразу по тьюплу с цветами радуги.
point = sd.get_point(600, -150)
radius = 300
width = 50
def rainbow(point, radius, color, width):
    sd.circle(center_position=point, radius=radius, color=color, width=width)

for i in rainbow_colors[::-1]:
    rainbow(point, radius, i, width)
    radius += width

# Усложненное задание, делать по желанию.
# Нарисовать радугу дугами от окружности (cсм sd.circle) за нижним краем экрана,
# поэкспериментировать с параметрами, что бы было красиво
sd.pause()