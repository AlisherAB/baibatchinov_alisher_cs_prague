# TODO Задание решить с примененением цикла после того как сделали без.

# Задание точно такое же как и без цикла.
def if_value_in(d, val):
    for key, value in d.items():
        if value == val:
            return key
# Есть словарь кодов товаров

goods = {
    'Лампа': '12345',
    'Стол': '23456',
    'Диван': '34567',
    'Стул': '45678',
}

# Есть словарь списков количества товаров на складе.

store = {
    '12345': [
        {'quantity': 27, 'price': 42},
    ],
    '23456': [
        {'quantity': 22, 'price': 510},
        {'quantity': 32, 'price': 520},
    ],
    '34567': [
        {'quantity': 2, 'price': 1200},
        {'quantity': 1, 'price': 1150},
    ],
    '45678': [
        {'quantity': 50, 'price': 100},
        {'quantity': 12, 'price': 95},
        {'quantity': 43, 'price': 97},
    ],
}

for i in store:
    quantity = 0
    price = 0
    for j in range(len(store[str(i)])):
        quantity += (store[str(i)][j])['quantity']
        price += quantity * (store[str(i)][j])['price']
    name = if_value_in(goods, str(i))
    print(name, '-', quantity, 'шт, стоимсоть', price, 'руб')
