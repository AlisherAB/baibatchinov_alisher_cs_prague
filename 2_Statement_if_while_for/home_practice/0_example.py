import random

import simple_draw as sd

sd.resolution = (1200, 600)

# Нарисовать пузырек - три вложенных окружностей с шагом 5 пикселей
# point = sd.get_point(300, 300)
# radius = 50
# for _ in range(3):
#    radius += 10
#    sd.circle(center_position=point, radius=radius, width=3)

# Написать функцию рисования пузырька, принммающую 3 (или более) параметра: точка рисования, шаг и цвет
def bubble(point, step, color):
    radius = 50
    for _ in range(3):
        radius += step
        sd.circle(center_position=point, radius=radius, color=color)

# point = sd.get_point(400, 400)
# bubble(60, 15, 5)


# Нарисовать 10 пузырьков в ряд
#for x in range(100, 1100, 100):
#     point = sd.get_point(x, 100)
#     bubble(point, step=5, color=sd.COLOR_CYAN)

# Нарисовать три ряда по 10 пузырьков
#for y in range(100, 301, 100):
#   for x in range(100, 1100, 100):
#       point = sd.get_point(x, y)
#       bubble(point, step=5, color=sd.COLOR_GREEN)


# Нарисовать 100 пузырьков в произвольных местах экрана случайными цветами

for _ in range(100):
    point = sd.random_point()
    step = random.randint(2, 10)
    width = random.randint(1, 10)

    bubble(point=point, step=step, color=sd.COLOR_RED)
sd.pause()