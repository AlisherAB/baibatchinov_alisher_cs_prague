# -*- coding: utf-8 -*-
import simple_draw as sd
sd.resolution = (1200, 900)
# Запросить у пользователя желаемую фигуру посредством выбора из существующих
#   вывести список всех фигур с номерами и ждать ввода номера желаемой фигуры.
# и нарисовать эту фигуру в центре экрана

# Код функций из упр 02_global_color.py скопировать сюда
# Результат решения см results/exercise_03_shape_select.jpg

def vector_drawing(angle_numbers, start_point, length):
    end_point = start_point
    for i in range(angle_numbers-1):
        v = sd.get_vector(start_point=end_point, angle=0+(360//angle_numbers)*i, length=length, width=3)
        v.draw()
        end_point = v.end_point
    sd.line(start_point=start_point, end_point=end_point, width=3)

def triangle(start_point, length):
    angle_numbers = 3
    vector_drawing(angle_numbers=angle_numbers, length=length, start_point=start_point)

def rectangle(start_point, length):
    angle_numbers = 4
    vector_drawing(angle_numbers=angle_numbers, length=length, start_point=start_point)

def pentangle(start_point, length):
    angle_numbers = 5
    vector_drawing(angle_numbers=angle_numbers, length=length, start_point=start_point)

def hexangle(start_point, length):
    angle_numbers = 6
    vector_drawing(angle_numbers=angle_numbers, length=length, start_point=start_point)

print('Возможные фигуры:\n', '    0 : треугольник\n', '    1 : квадрат\n',
      '    2 : пятиугольник\n', '    3 : шестиугольник\n',)

while True:
    shape = int(input('Выберите желаемую фигуру > '))
    if shape < 0 or shape > 3:
        print('Вы ввели нкорректный номер')
    else:
        break

point = sd.get_point(500, 450)
if shape == 0:
    triangle(start_point=point, length=200)
elif shape == 1:
    rectangle(start_point=point, length=200)
elif shape == 2:
    pentangle(start_point=point, length=200)
elif shape == 3:
    hexangle(start_point=point, length=200)

sd.pause()