# -*- coding: utf-8 -*-
import simple_draw as sd
sd.background_color = sd.COLOR_BLACK
sd.resolution = (1200, 800)
# Добавить цвет в функции рисования геом. фигур. из упр 01_shapes.py
# (код функций скопировать сюда и изменить)
# Запросить у пользователя цвет фигуры посредством выбора из существующих:
#   вывести список всех цветов с номерами и ждать ввода номера желаемого цвета.
# Потом нарисовать все фигуры этим цветом

# Пригодятся функции
# sd.get_point()
# sd.line()
# sd.get_vector()
# и константы COLOR_RED, COLOR_ORANGE, COLOR_YELLOW, COLOR_GREEN, COLOR_CYAN, COLOR_BLUE, COLOR_PURPLE
# Результат решения см results/exercise_02_global_color.jpg

# TODO здесь ваш код
def vector_drawing(angle_numbers, start_point, length, color):
    end_point = start_point
    for i in range(angle_numbers-1):
        v = sd.get_vector(start_point=end_point, angle=0+(360//angle_numbers)*i, length=length, width=3)
        v.draw(color=color)
        end_point = v.end_point
    sd.line(start_point=start_point, end_point=end_point, width=3, color=color)

def triangle(start_point, length, color):
    angle_numbers = 3
    vector_drawing(angle_numbers=angle_numbers, length=length, start_point=start_point, color=color)

def rectangle(start_point, length, color):
    angle_numbers = 4
    vector_drawing(angle_numbers=angle_numbers, length=length, start_point=start_point, color=color)

def pentangle(start_point, length, color):
    angle_numbers = 5
    vector_drawing(angle_numbers=angle_numbers, length=length, start_point=start_point, color=color)

def hexangle(start_point, length, color):
    angle_numbers = 6
    vector_drawing(angle_numbers=angle_numbers, length=length, start_point=start_point, color=color)

print('Возможные цвета:\n', '    0 : red\n', '    1 : orange\n', '    2 : yellow\n', '    3 : green\n',
          '    4 : cyan\n', '    5 : blue\n', '    6 : purple')
while True:
    clr = int(input('Введимте желаемый цвет > '))
    if clr < 0 or clr > 6:
        print('Вы ввели нкорректный номер')
    else:
        break

colors = (sd.COLOR_RED, sd.COLOR_ORANGE, sd.COLOR_YELLOW, sd.COLOR_GREEN,
                  sd.COLOR_CYAN, sd.COLOR_BLUE, sd.COLOR_PURPLE)

point = sd.get_point(300, 200)
point2 = sd.get_point(800, 200)
point3 = sd.get_point(300, 500)
point4 = sd.get_point(800, 500)

triangle(start_point=point, length=200, color=colors[clr])
rectangle(start_point=point2, length=200, color=colors[clr])
pentangle(start_point=point3, length=150, color=colors[clr])
hexangle(start_point=point4, length=150, color=colors[clr])

sd.pause()