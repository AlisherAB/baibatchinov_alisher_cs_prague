# -*- coding: utf-8 -*-

import simple_draw as sd

sd.resolution = (1200, 900)


# Часть 1.
# Написать функции рисования равносторонних геометрических фигур:
# - треугольника
# - квадрата
# - пятиугольника
# - шестиугольника
# Все функции должны принимать 3 параметра:
# - точка начала рисования
# - угол наклона
# - длина стороны
#
# Примерный алгоритм внутри функции:
#   # будем рисовать с помощью векторов, каждый следующий - из конечной точки предыдущего
#   текущая_точка = начальная точка
#   для угол_наклона из диапазона от 0 до 360 с шагом XXX
#      # XXX подбирается индивидуально для каждой фигуры
#      составляем вектор из текущая_точка заданной длины с наклоном в угол_наклона
#      рисуем вектор
#      текущая_точка = конечной точке вектора
#
# Использование копи-пасты - обязательно! Даже тем кто уже знает про её пагубность. Для тренировки.
# Как работает копипаста:
#   - одну функцию написали,
#   - копипастим её, меняем название, чуть подправляем код,
#   - копипастим её, меняем название, чуть подправляем код,
#   - и так далее.
# В итоге должен получиться ПОЧТИ одинаковый код в каждой функции

# Пригодятся функции
# sd.get_point()
# sd.get_vector()
# sd.line()
# Результат решения см /results/exercise_01_shapes.jpg

'''def triangle(start_point, length, angle):
    v1 = sd.get_vector(start_point=start_point, angle=angle, length=length, width=3)
    v1.draw()
    v2 = sd.get_vector(start_point=v1.end_point, angle=angle+120, length=length, width=3)
    v2.draw()
    v3 = sd.get_vector(start_point=v2.end_point, angle=angle+240, length=length, width=3)
    v3.draw()

def rectangle(start_point, length, angle):
    v1 = sd.get_vector(start_point=start_point, angle=angle, length=length, width=3)
    v1.draw()
    v2 = sd.get_vector(start_point=v1.end_point, angle=angle+90, length=length, width=3)
    v2.draw()
    v3 = sd.get_vector(start_point=v2.end_point, angle=angle+180, length=length, width=3)
    v3.draw()
    v4 = sd.get_vector(start_point=v3.end_point, angle=angle+270, length=length, width=3)
    v4.draw()

def pentangle(start_point, length, angle):
    v1 = sd.get_vector(start_point=start_point, angle=angle, length=length, width=3)
    v1.draw()
    v2 = sd.get_vector(start_point=v1.end_point, angle=angle+72, length=length, width=3)
    v2.draw()
    v3 = sd.get_vector(start_point=v2.end_point, angle=angle+144, length=length, width=3)
    v3.draw()
    v4 = sd.get_vector(start_point=v3.end_point, angle=angle+216, length=length, width=3)
    v4.draw()
    v5 = sd.get_vector(start_point=v4.end_point, angle=angle + 288, length=length, width=3)
    v5.draw()

def heptangle(start_point, length, angle):
    v1 = sd.get_vector(start_point=start_point, angle=angle, length=length, width=3)
    v1.draw()
    v2 = sd.get_vector(start_point=v1.end_point, angle=angle+60, length=length, width=3)
    v2.draw()
    v3 = sd.get_vector(start_point=v2.end_point, angle=angle+120, length=length, width=3)
    v3.draw()
    v4 = sd.get_vector(start_point=v3.end_point, angle=angle+180, length=length, width=3)
    v4.draw()
    v5 = sd.get_vector(start_point=v4.end_point, angle=angle+240, length=length, width=3)
    v5.draw()
    v6 = sd.get_vector(start_point=v5.end_point, angle=angle+300, length=length, width=3)
    v6.draw()

point = sd.get_point(50, 300)
triangle(start_point=point, length=200, angle=0)
point2 = sd.get_point(50, 600)
rectangle(start_point=point2, length=200, angle=0)
point3 = sd.get_point(300, 300)
pentangle(start_point=point3, length=200, angle=0)
point4 = sd.get_point(700, 400)
heptangle(start_point=point4, length=200, angle=0)'''
# Часть 1-бис.
# Попробуйте прикинуть обьем работы, если нужно будет внести изменения в этот код.
# Скажем, связывать точки не линиями, а дугами. Или двойными линиями. Или рисовать круги в угловых точках. Или...
# А если таких функций не 4, а 44? Код писать не нужно, просто представь объем работы... и запомни это.
# зачет

# Часть 2 (делается после зачета первой части)
#
# Надо сформировать функцию, параметризированную в местах где была "небольшая правка".
# Это называется "Выделить общую часть алгоритма в отдельную функцию"
# Потом надо изменить функции рисования конкретных фигур - вызывать общую функцию вместо "почти" одинакового кода.
#
# В итоге должно получиться:
#   - одна общая функция со множеством параметров,
#   - все функции отрисовки треугольника/квадрата/етс берут 3 параметра и внутри себя ВЫЗЫВАЮТ общую функцию.
#
# Не забудте в этой общей функции придумать, как устранить разрыв в начальной/конечной точках рисуемой фигуры
# (если он есть. подсказка - на последней итерации можно использовать линию от первой точки)

def vector_drawing(angle_numbers, start_point, length):
    end_point = start_point
    for i in range(angle_numbers-1):
        v = sd.get_vector(start_point=end_point, angle=0+(360//angle_numbers)*i, length=length, width=3)
        v.draw()
        end_point = v.end_point
    sd.line(start_point=start_point, end_point=end_point, width=3)

# def shape(start_point, angle_numbers, length, color=None):
#    vector_drawing(angle_numbers=angle_numbers, length=length, start_point=start_point)

def triangle(start_point, angle, length):
    angle_numbers = 3
    vector_drawing(angle_numbers=angle_numbers, length=length, start_point=start_point)

def rectangle(start_point, angle, length):
    angle_numbers = 4
    vector_drawing(angle_numbers=angle_numbers, length=length, start_point=start_point)

def pentangle(start_point, angle, length):
    angle_numbers = 5
    vector_drawing(angle_numbers=angle_numbers, length=length, start_point=start_point)

def hexangle(start_point, angle, length):
    angle_numbers = 6
    vector_drawing(angle_numbers=angle_numbers, length=length, start_point=start_point)

point = sd.get_point(300, 300)
point2 = sd.get_point(800, 300)
point3 = sd.get_point(300, 600)
point4 = sd.get_point(800, 600)
#shape(start_point=point, length=100, angle_numbers=10)
triangle(start_point=point, angle=0, length=200)
rectangle(start_point=point2, angle=0, length=200)
pentangle(start_point=point3, angle=0, length=150)
hexangle(start_point=point4, angle=0, length=150)



# Часть 2-бис.
# А теперь - сколько надо работы что бы внести изменения в код? Выгода на лицо :)
# Поэтому среди программистов есть принцип D.R.Y. https://clck.ru/GEsA9
# Будьте ленивыми, не используйте копи-пасту!


sd.pause()