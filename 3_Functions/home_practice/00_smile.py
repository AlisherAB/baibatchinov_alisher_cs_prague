# (определение функций)
import simple_draw as sd
import random
sd.resolution = (1200, 600)


# Написать функцию отрисовки смайлика по заданным координатам
# Форма рожицы-смайлика на ваше усмотрение
# Параметры функции: кордината X, координата Y, цвет.
# Вывести 10 смайликов в произвольных точках экрана.

for i in range(30):
    x = random.randint(50, 1150)
    y = random.randint(50, 550)
    point = sd.get_point(x, y)
    sd.circle(center_position=point, radius=51, color=sd.COLOR_BLACK, width=1)
    sd.circle(center_position=point, radius=50, color=[255, 170, 0], width=50)
    eye1 = sd.get_point(x - 22, y + 10)
    eye2 = sd.get_point(x + 22, y + 10)
    sd.circle(center_position=eye1, radius=9, color=sd.COLOR_BLACK, width=9)
    sd.circle(center_position=eye2, radius=9, color=sd.COLOR_BLACK, width=9)
    l_b = sd.get_point(x - 16, y - 35)
    r_t = sd.get_point(x + 16, y - 15)
    sd.ellipse(left_bottom=l_b, right_top=r_t, color=sd.COLOR_WHITE, width=20)
    l_b = sd.get_point(x - 16, y - 12)
    r_t = sd.get_point(x + 16, y - 5)
    sd.rectangle(left_bottom=l_b, right_top=r_t, color=[255, 170, 0], width=10)

sd.pause()