
# (определение функций)
import simple_draw as sd
import random
sd.resolution = (1360, 700)


# Создать функцию которая нарисует шахматную доску по двум параметрам.

# Функцию следующего вида def desk(yy, xx):
# Где уу = высота доски(количество квадратов по вертикали), хх = ширина доски(количесво квадратов по горизонтали)

# Я добавил в функцию параметр length, отвечающий за длину клетки шахматной доски, думаю это не критично,
# в противном случае я бы просто убрал параметр length из функции и заменил его в коде на какое-нибудь константное
# чсило, например 50, что бы означало, что клетки будут иметь константную длину 50, которую пользователь
# сам не выбирает.

# Сделал сначала через sd.rectangle, через sd.square немного проще
'''def desk(yy, xx, length):
    x_bottom, y_bottom = 10, 10
    x_top, y_top = 10 + length, 10 + length
    point = sd.get_point(x_bottom, y_bottom)
    for i in range(1, yy + 1):
        for j in range(1, xx + 1):
            if i % 2 != 0 and j % 2 == 0 or i % 2 == 0 and j % 2 != 0:
                color = sd.COLOR_BLACK
            else:
                color = sd.COLOR_WHITE
            sd.rectangle(left_bottom=point, right_top=sd.get_point(x_top, y_top), color=color, width=0)
            x_bottom += length
            x_top += length
            point = sd.get_point(x_bottom, y_bottom)
        x_bottom = 10
        y_bottom += length
        y_top += length
        x_top = 10 + length
        point = sd.get_point(x_bottom, y_bottom)'''

def desk(yy, xx, length):
    x_bottom, y_bottom = 10, 10
    point = sd.get_point(x_bottom, y_bottom)
    for i in range(1, yy + 1):
        for j in range(1, xx + 1):
            if i % 2 != 0 and j % 2 == 0 or i % 2 == 0 and j % 2 != 0:
                color = sd.COLOR_BLACK
            else:
                color = sd.COLOR_WHITE
            sd.square(left_bottom=point, side=length, color=color, width=0)
            x_bottom += length
            point = sd.get_point(x_bottom, y_bottom)
        x_bottom = 10
        y_bottom += length
        point = sd.get_point(x_bottom, y_bottom)

x, y, length = [int(i) for i in input('Введите длину и ширину шахматной доски в клетках,'
                                      ' а затем выберите длину самой клетки шахматной доски: ').split()]
desk(yy=y, xx=x, length=length)
sd.pause()