# -*- coding: utf-8 -*-
import simple_draw as sd
sd.resolution = (1200, 600)
# На основе кода из практической части реализовать снегопад:
# - создать списки данных для отрисовки N снежинок
# - нарисовать падение этих N снежинок
# - создать список рандомных длинн лучей снежинок (от 10 до 100) и пусть все снежинки будут разные

N = 20
x_point = []
y_point = []
'''for i in range(N):
    x_point.append(sd.randint(0, 1200))
    y_point.append((sd.randint(400, 600)))
length = []
while len(length) != 20:
    a = sd.randint(10, 100)
    if a not in length:
        length.append(a)
# Пригодятся функции
# sd.get_point()
# sd.snowflake()
# sd.sleep()
# sd.random_number()
# sd.user_want_exit()


# Примерный алгоритм отрисовки снежинок
#   навсегда
#     очистка экрана
#     для индекс, координата_х из списка координат снежинок
#       получить координата_у по индексу
#       создать точку отрисовки снежинки
#       нарисовать снежинку цветом фона
#       изменить координата_у и запомнить её в списке по индексу
#       создать новую точку отрисовки снежинки
#       нарисовать снежинку на новом месте белым цветом
#     немного поспать
#     если пользователь хочет выйти
#       прервать цикл


for i in range(N):
    while True:
        sd.clear_screen()
        point = sd.get_point(x_point[i], y_point[i])
        sd.snowflake(center=point, length=length[i])
        y_point[i] -= 10
        if y_point[i] < 0:
            break
        #x_point[i] = x_point[i] + 5
        sd.sleep(0.05)
        if sd.user_want_exit():
            break

sd.pause()'''
# Часть 2 (делается после зачета первой части)
#
# Ускорить отрисовку снегопада
# - убрать clear_screen() из цикла
# - в начале рисования всех снежинок вызвать sd.start_drawing()
# - на старом месте снежинки отрисовать её же, но цветом sd.background_color
# - сдвинуть снежинку
# - отрисовать её цветом sd.COLOR_WHITE на новом месте
# - после отрисовки всех снежинок, перед sleep(), вызвать sd.finish_drawing()

'''for i in range(N):
    x_point.append(sd.randint(0, 1200))
    y_point.append((sd.randint(400, 600)))
length = []
while len(length) != 20:
    a = sd.randint(10, 100)
    if a not in length:
        length.append(a)

for i in range(N):
    while True:
        point = sd.get_point(x_point[i], y_point[i])
        sd.start_drawing()
        sd.snowflake(center=point, length=length[i])
        sd.snowflake(center=sd.get_point(x_point[i], y_point[i]+10), length=length[i], color=sd.background_color)
        y_point[i] -= 10
        if y_point[i] < 10:
            break
        #x_point[i] = x_point[i] + 5
        sd.finish_drawing()
        sd.sleep(0.01)
        if sd.user_want_exit():
            break

sd.pause()'''

# Усложненное задание (делать по желанию)
# - сделать рандомные отклонения вправо/влево при каждом шаге
# - сделать сугроб внизу экрана - если снежинка долетает до низа, оставлять её там,
#   и добавлять новую снежинку
# TODO немного не понял насчет сугроба, как конкретно он должен выглядеть? Снежинки у нас и так
#  остаются после того как приземляться. Должны ли они проходить сквозь друг друга или копиться друг
#  на друге в виде горы?
for i in range(N):
    x_point.append(sd.randint(0, 1200))
    y_point.append((sd.randint(400, 600)))
length = []
while len(length) != 20:
    a = sd.randint(10, 100)
    if a not in length:
        length.append(a)

for i in range(N):
    x_delta = sd.randint(-10, 10)
    while True:
        point = sd.get_point(x_point[i], y_point[i])
        sd.start_drawing()
        sd.snowflake(center=point, length=length[i])
        sd.snowflake(center=sd.get_point(x_point[i] - x_delta, y_point[i]+10), length=length[i], color=sd.background_color)
        y_point[i] -= 10
        if y_point[i] < length[i] - 10:
            break
        x_point[i] = x_point[i] + x_delta
        sd.finish_drawing()
        sd.sleep(0.02)
        if sd.user_want_exit():
            break

sd.pause()
