import simple_draw as sd
sd.resolution = (1600, 800)

def desk(yy, xx):
    y_mult = yy * 100 + 1
    x_mult = xx * 100 + 1
    for step in range(100, 201, 100):
        for y in range(step, y_mult, 200):
            for x in range(step, x_mult, 200):
                point = sd.get_point(x, y)
                sd.square(point, side=100, width=0, color=sd.COLOR_BLACK)
    for y in range(100, y_mult, 200):
        for x in range(200, x_mult, 200):
            point = sd.get_point(x, y)
            sd.square(point, side=100, width=0, color=sd.COLOR_WHITE)
    for y in range(200, y_mult, 200):
        for x in range(100, x_mult, 200):
            point = sd.get_point(x, y)
            sd.square(point, side=100, width=0, color=sd.COLOR_WHITE)

desk(4, 8)

# for step in range(100, 201, 100):
#     for y in range(step, 801, 200):
#         for x in range(step, 1001, 200):
#             point = sd.get_point(x, y)
#             sd.square(point, side=100, width=0, color=sd.COLOR_BLACK)
#
# # for y in range(200, 801, 200):
# #     for x in range(200, 1001, 200):
# #         point = sd.get_point(x, y)
# #         sd.square(point, side=100, width=0, color=sd.COLOR_BLACK)
#
# for y in range(100, 801, 200):
#     for x in range(200, 1001, 200):
#         point = sd.get_point(x, y)
#         sd.square(point, side=100, width=0, color=sd.COLOR_WHITE)
#
# for y in range(200, 801, 200):
#     for x in range(100, 1001, 200):
#         point = sd.get_point(x, y)
#         sd.square(point, side=100, width=0, color=sd.COLOR_WHITE)

sd.pause()