import simple_draw as sd

sd.resolution = (1200, 600)
colors = (sd.COLOR_RED, sd.COLOR_ORANGE, sd.COLOR_YELLOW, sd.COLOR_GREEN, sd.COLOR_CYAN,
          sd.COLOR_BLUE, sd.COLOR_PURPLE)
for i in range(1, 8):
    sd.circle(center_position=sd.get_point(0,-600), radius=1550 - i * 10, color=colors[i-1], width=10)

sd.pause()