import simple_draw as sd
sd.resolution = (1200, 600)

sd.circle(center_position=sd.get_point(600, 300), radius=50, width=0, color=sd.COLOR_WHITE)
sd.circle(center_position=sd.get_point(650, 300), radius=35, width=0, color=sd.COLOR_WHITE)
sd.circle(center_position=sd.get_point(550, 300), radius=35, width=0, color=sd.COLOR_WHITE)

sd.pause()