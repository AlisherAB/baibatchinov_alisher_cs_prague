import simple_draw as sd

def snowfall():
    while True:
        x_point = (sd.randint(300, 800))
        y_point = (sd.randint(500, 600))
        length = sd.randint(10, 30)
        while True:
            point = sd.get_point(x_point, y_point)
            sd.start_drawing()
            sd.snowflake(center=point, length=length)
            sd.snowflake(center=sd.get_point(x_point, y_point), length=length, color=sd.background_color)
            y_point -= 10
            if y_point < length - 10:
                break
            x_point = x_point + 10
            sd.finish_drawing()
            sd.sleep(0.02)
            if sd.user_want_exit():
                break

