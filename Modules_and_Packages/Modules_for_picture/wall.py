import simple_draw as sd

def house():
    x_start, x_end, y_length = sd.get_point(300, 100), sd.get_point(600, 100), 150
    brick_x, brick_y = 30, 15
    x_point, y_point = x_start.x, x_start.y
    x_end_point, y_end_point = x_start.x + brick_x, y_point + brick_y

    v1 = sd.get_vector(start_point=x_start, angle=0, length=(x_end.x - x_start.x) + brick_x//2, width=1)
    v1.draw()
    v2 = sd.get_vector(start_point=v1.end_point, angle=90, length=y_length,
                       width=1)
    v2.draw()
    v3 = sd.get_vector(start_point=v2.end_point, angle=180,
                       length=(x_end.x - x_start.x) + brick_x//2, width=1)
    v3.draw()
    v4 = sd.get_vector(start_point=v3.end_point, angle=270, length=y_length, width=1)
    v4.draw()

    for i in range(y_length // brick_y):
        if i % 2 == 0:
            x_point = x_point + brick_x//2
            x_end_point = x_end_point + brick_x//2
        l_b = sd.get_point(x_point, y_point)
        r_t = sd.get_point(x_end_point, y_end_point)
        for j in range((x_end.x-x_start.x) // brick_x):
            sd.rectangle(left_bottom=l_b, right_top=r_t, color=sd.COLOR_YELLOW, width=1)
            x_point += brick_x
            l_b = sd.get_point(x_point, y_point)
            x_end_point += brick_x
            r_t = sd.get_point(x_end_point, y_end_point)
        x_point = x_start.x
        x_end_point = x_start.x + brick_x
        y_point += brick_y
        y_end_point += brick_y

        def roof():
            xs = x_start.x - brick_x
            xe = x_end.x + brick_x * 1.5
            '''v1 = sd.get_vector(start_point=sd.get_point(xs, y_length+100), angle=0, length=xe-xs, width=1)
            v1.draw(color=sd.COLOR_RED)
            sd.line(start_point=sd.get_point(xs, y_length+100), end_point=sd.get_point(xs + (xe-xs)//2, 300),
                    width=1, color=sd.COLOR_RED)
            sd.line(start_point=sd.get_point(xs + (xe-xs)//2, 300), end_point=v1.end_point,
                    width=1, color=sd.COLOR_RED)'''
            roof_x_start = xs + 4
            roof_x_end = xe - 4
            roof_y_start = y_length + 101
            while roof_x_start <= roof_x_end:
                v1 = sd.get_vector(start_point=sd.get_point(roof_x_start, roof_y_start),
                                   length=roof_x_end - roof_x_start, angle=0)
                v1.draw(color=sd.COLOR_RED)
                roof_x_start += 4
                roof_x_end -= 4
                roof_y_start += 1
        roof()

        def window():
            start = sd.get_point(494, 129)
            sd.square(left_bottom=start, side=106, color=sd.COLOR_CYAN)
            sd.square(left_bottom=start, side=106, color=sd.COLOR_YELLOW, width=1)
        window()
