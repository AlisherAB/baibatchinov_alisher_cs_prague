#start_point, angle, length
import simple_draw as sd

def tree(start_point, angle, length):
    if length < 20:
        width = 2
    else:
        width = 3
    if length < 5:
        return
    v1 = sd.get_vector(start_point=start_point, angle=angle, length=length, width=width)
    if length < 20:
        v1.draw(color=sd.COLOR_GREEN)
    else:
        v1.draw(color=sd.COLOR_DARK_ORANGE)
    next_point = v1.end_point
    next_angle1 = v1.angle - (30 * (1 + (sd.random_number(-40, 40) / 100)))
    next_angle2 = v1.angle + (30 * (1 + (sd.random_number(-40, 40) / 100)))
    next_length = length * 0.75 * (1 + (sd.random_number(0, 10)) / 100)
    tree(start_point=next_point, angle=next_angle1, length=next_length)
    tree(start_point=next_point, angle=next_angle2, length=next_length)

