import simple_draw as sd

def smile():
    x = 549
    y = 182
    point = sd.get_point(x, y)
    sd.circle(center_position=point, radius=26, color=sd.COLOR_BLACK, width=1)
    sd.circle(center_position=point, radius=25, color=[255, 170, 0], width=50)
    eye1 = sd.get_point(x - 22*0.5, y + 10*0.5)
    eye2 = sd.get_point(x + 22*0.5, y + 10*0.5)
    sd.circle(center_position=eye1, radius=4, color=sd.COLOR_BLACK, width=4)
    sd.circle(center_position=eye2, radius=4, color=sd.COLOR_BLACK, width=4)
    l_b = sd.get_point(x - 16*0.5, y - 35*0.5)
    r_t = sd.get_point(x + 16*0.5, y - 15*0.5)
    sd.ellipse(left_bottom=l_b, right_top=r_t, color=sd.COLOR_WHITE, width=20)
    l_b = sd.get_point(x - 16*0.5, y - 12*0.5)
    r_t = sd.get_point(x + 16*0.5, y - 5*0.5)
    sd.rectangle(left_bottom=l_b, right_top=r_t, color=[255, 170, 0], width=10)
