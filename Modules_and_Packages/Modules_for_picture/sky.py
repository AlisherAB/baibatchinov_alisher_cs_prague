import simple_draw as sd

def sky():
    sd.circle(center_position=sd.get_point(600, 550), radius=50, width=0, color=sd.COLOR_WHITE)
    sd.circle(center_position=sd.get_point(650, 550), radius=35, width=0, color=sd.COLOR_WHITE)
    sd.circle(center_position=sd.get_point(550, 550), radius=35, width=0, color=sd.COLOR_WHITE)

    sd.circle(center_position=sd.get_point(750, 500), radius=40, width=0, color=sd.COLOR_WHITE)
    sd.circle(center_position=sd.get_point(790, 500), radius=25, width=0, color=sd.COLOR_WHITE)
    sd.circle(center_position=sd.get_point(710, 500), radius=23, width=0, color=sd.COLOR_WHITE)

    sd.circle(center_position=sd.get_point(400, 525), radius=35, width=0, color=sd.COLOR_WHITE)
    sd.circle(center_position=sd.get_point(435, 525), radius=20, width=0, color=sd.COLOR_WHITE)
    sd.circle(center_position=sd.get_point(365, 525), radius=20, width=0, color=sd.COLOR_WHITE)
