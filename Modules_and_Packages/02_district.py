# -*- coding: utf-8 -*-

# Составить список всех живущих на районе и Вывести на консоль через запятую
# Формат вывода: На районе живут ...
# подсказка: для вывода элементов списка через запятую можно использовать функцию строки .join()
# https://docs.python.org/3/library/stdtypes.html#str.join

from Modules_and_Packages import room_1
from Modules_and_Packages import room_2

district = room_1.folks + room_2.folks
print('На районе живут:', ', '.join(district))

