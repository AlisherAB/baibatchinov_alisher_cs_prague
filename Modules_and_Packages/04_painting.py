# -*- coding: utf-8 -*-
import simple_draw as sd
from Modules_for_picture import ground
from Modules_for_picture import rainbow
from Modules_for_picture import sun
from Modules_for_picture import wall
from Modules_for_picture import tree
from Modules_for_picture import sky
from Modules_for_picture import smile
from Modules_for_picture import snowfall


sd.resolution = (1200, 600)

ground.ground()

rainbow.rainbow()

start_point, angle, length = sd.get_point(800, 100), 90, 60
tree.tree(start_point, angle, length)

start_point, angle, length = sd.get_point(1100, 100), 90, 40
tree.tree(start_point, angle, length)

sky.sky()

wall.house()

smile.smile()


sd.pause()


# Создать пакет, в который скопировать функции отрисовки из предыдущего урока
#  - радуги
#  - стены
#  - дерева
#  - смайлика
#  - снежинок
# Функции по модулям разместить по тематике. Название пакета и модулей - по смыслу.
# Создать модуль с функцией отрисовки кирпичного дома с широким окном и крышей.

# С помощью созданного пакета нарисовать эпохальное полотно "Утро в деревне".
# На картине должны быть:
#  - кирпичный дом, в окошке - смайлик.
#  - слева от дома - сугроб (предположим что это ранняя весна)
#  - справа от дома - дерево (можно несколько)
#  - справа в небе - радуга, слева - солнце (весна же!)
# Приправить своей фантазией по вкусу (коты? коровы? люди? трактор? что придумается)
# пример см. results/04_painting.jpg
# **************************************************
# Усложненное задание.
# Анимировать картину.
# Пусть слева идет снегопад, радуга переливается цветами, смайлик моргает, солнце крутит лучами, етс.
# Задержку в анимировании все равно надо ставить, пусть даже 0.01 сек - так библиотека устойчивей работает.
