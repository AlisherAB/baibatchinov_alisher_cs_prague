# -*- coding: utf-8 -*-
from Modules_and_Packages import my_burger as mb
# Создать модуль my_burger. В нем определить функции добавления инградиентов:
#  - булочки
#  - котлеты
#  - огурчика
#  - помидорчика
#  - майонеза
#  - сыра
# В каждой функции выводить на консоль что-то вроде "А теперь добавим ..."

# В этом модуле создать рецепт двойного чизбургера (https://goo.gl/zA3goZ)
# с помощью фукций из my_burger и вывести на консоль.

mb.lower_bun(), mb.majonez(), mb.beef(), mb.chees(), mb.beef(), mb.chees()
mb.pickle(), mb.tomato(), mb.majonez(), mb.upper_bun()

# Создать рецепт своего бургера, по вашему вкусу.
# Если не хватает ингридиентов - создать соответствующие функции в модуле my_burger
print('\n', 'Рецепт моего бургера:', '\n')
mb.lower_bun(), mb.majonez(), mb.pickle(), mb.lettuce(), mb.tomato()
mb.onion(), mb.beef(), mb.chees(), mb.upper_bun()